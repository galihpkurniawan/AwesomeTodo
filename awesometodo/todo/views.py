import logging
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.utils.timezone import now
from django.contrib.auth.decorators import login_required
from rest_framework import viewsets
from .models import Task
from .forms import TaskForm, TaskModelForm
from .serializers import TaskSerializer
from rest_framework import filters
from django_filters.rest_framework import DjangoFilterBackend

logger = logging.getLogger(__package__)


@login_required
def home(request):
    logger.debug("write debugging message here")
    tasks = Task.objects.all()
    logger.debug("jumlah task = {}".format(len(tasks)))
    time_now = now()
    return render(request, "home.html", {"tasks": tasks, "now": time_now})


@login_required
def add_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        # form_datapribadi = DataPribadiForm(request.POST)
        # form_datakeluarga = DataKeluargaForm(request.POST)
        if form.is_valid():
            import pdb

            # pdb.set_trace()
            task_name = form.cleaned_data["name"]
            task = Task(name=task_name)
            task.save()
            return redirect("/")
    else:
        form = TaskForm()
    return render(request, "add.html", {"form": form})


@login_required
def add_task_model_form(request):
    if request.method == "POST":
        form = TaskModelForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/")
    else:
        form = TaskModelForm()
    return render(request, "add.html", {"form": form})


def user_profile(request):
    return render(request, "profile.html")


class TaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    filter_backends = [DjangoFilterBackend, filters.SearchFilter]
    filter_fields = ["name", "description", "is_done"]
    search_fields = ["name", "description"]
