from django.db import models
from django.utils import timezone


class Task(models.Model):
    name = models.CharField(max_length=400)
    description = models.TextField(blank=True)
    is_done = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deadline_at = models.DateTimeField()

    def is_late(self):
        now = timezone.now()
        return self.deadline_at < now and not self.is_done

    def __str__(self):
        return "(Task {})".format(self.name)
